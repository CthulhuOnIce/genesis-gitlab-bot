# Genesis GitLab Bot

The bot used for issue / merge request management and organization on the Genesis GitLab page.

**You need the `savedata.ini` and `logfile.txt` created and in the same folder as `main.py`**

The following python libraries must be installed.
 - requests
 - simplejson

Also you need to have a gitlab account with a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the "api" option enabled.

It also needs to be a manager of whatever repo you're assigning it to.