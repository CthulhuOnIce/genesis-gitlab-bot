import requests
import time
import simplejson as json

config = {
	"token": "Your token",
	"project-id": "your pid",
	"timeout": 10 # time to wait between scans
}

c = config

del config  # the config dict is only there to make it easy to understand, it's converted to 'c' to make it easy to read/write

root_url = f"https://gitlab.com/api/v4/projects/{c['project-id']}"
headers = {"PRIVATE-TOKEN": c['token']}

def find_in_issue(item, text):
	if text in item["description"]:
		return True
	if text in item["title"]:
		return True
	return False

def append_safe(item, value):
	if not value in item:
		item.append(value)
	return item

def process_issue(issue, labels):

	if issue['assignee'] != None:
		labels = append_safe(labels, "assigned")
		if "unassigned" in labels:
			labels.remove("unassigned")
	else:
		labels = append_safe(labels, "unassigned")
		if "assigned" in labels:
			labels.remove("assigned")

	if find_in_issue(issue, "sprite") or find_in_issue(issue, "spriting"):
		labels = append_safe(labels, "spriting")
	if find_in_issue(issue, "map"):
		labels = append_safe(labels, "mapping")
	if find_in_issue(issue, "config"):
		labels = append_safe(labels, "config")
	if find_in_issue(issue, "discord"):
		labels = append_safe(labels, "discord")
	if find_in_issue(issue, "gitlab"):
		labels = append_safe(labels, "gitlab")
	if find_in_issue(issue, "lore"):
		labels = append_safe(labels, "lore")
	if find_in_issue(issue, "runtime"):
		labels = append_safe(labels, "runtime")

	return labels

def urlify(item):
	ret = ""
	for x in item:
		ret += f"{x},"
	return ret

def get(path):
	return requests.get(url=f"{root_url}{path}", headers = headers).json()

def put(path):
	return requests.put(url=f"{root_url}{path}", headers = headers).json()

def savejson(dat):
	with open("savedata.ini", "w") as w:
		w.write(json.dumps(dat))

def readjson():
	dat = None
	with open("savedata.ini", "r") as savedata:
		dat = savedata.read()
	if dat != "":
		return json.loads(dat)
	return None

def resolvediffs(dict1, dict2):
	dict3 = {"issues": []}
	for item1 in dict1:
		found = False
		changes = False
		issueid = item1['id']
		for item2 in dict2:
			if issueid == item2["id"]:
				found = True
				if item1['description'] != item2['description']:							changes = True
				if item1['assignees'] != item2['assignees']:								changes = True

		if not found:  # new issue found
			item1.update({"old_labels": item1["labels"][:]})  # the [:] is there so it's cloning the list instead of reffing it
			dict3["issues"].append(item1)
		if changes:		# old issue found, but it's changed
			item1.update({"old_labels": item1["labels"][:]})
			dict3["issues"].append(item1)

	return dict3

def log(text):
	with open("logfile.txt", "a") as a:
		a.write(f"{text}\n")
	print(text)

def should_update(j):
	old_labels = j["old_labels"]
	labels = j["labels"]
	if old_labels == labels:
		return False
	return True

def push_labels(iid, labels):
	put(f"/issues/{iid}?labels={urlify(labels)}")

def close_issue(iid):
	put(f"/issues/{iid}?state_event=close")

def finish_issue(issue):
	issue["labels"] = process_issue(issue, issue["labels"])
	if should_update(issue):
		log(f"Setting labels {issue['labels']} to issue '{issue['title']}' (#{issue['iid']})")
		push_labels(issue['iid'], issue['labels'])

while True:

	issues = get('/issues?state=opened&per_page=100')

	page = 2

	while True: # get any and all pages that exist 
		issuepage = get(f'/issues?state=opened&per_page=100&page={page}')
		if issuepage != []:
			issues += issuepage # merge them
		else:
			break

	save = readjson()

	savejson(issues)

	if save == None:
		savejson(issues)
		save = issues

	new_issues = resolvediffs(issues, save)

	for issue in new_issues["issues"]:
		if issue['description'].startswith("<!-- CUSTOM -->"):
			issue["labels"] = append_safe(issue["labels"], "custom")
			finish_issue(issue)

		elif issue['description'].startswith("<!-- BUG -->"):
			issue["labels"] = append_safe(issue["labels"], "bug")
			finish_issue(issue)

		elif issue['description'].startswith("<!-- ENHANCEMENT -->"):
			issue["labels"] = append_safe(issue["labels"], "enhancement")
			finish_issue(issue)

		elif issue['description'].startswith("<!-- META -->"):
			issue["labels"] = append_safe(issue["labels"], "meta")
			finish_issue(issue)

		else:
			issue["labels"] = []  # clear it
			issue["labels"] = append_safe(issue["labels"], "closed by bot - incorrect formatting")
			push_labels(issue['iid'], issue['labels'])
			close_issue(issue['iid'])
			log(f"Closed issue '{issue['title']}' (#{issue['iid']}) due to incorrect formatting.")
			
	time.sleep(c['timeout'])